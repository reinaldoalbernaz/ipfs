﻿### **VÍDEO EXPLICANDO IPFS E O TUTORIAL:** 
[https://www.youtube.com/watch?v=kXix9grA12M&feature=youtu.be](https://www.youtube.com/watch?v=kXix9grA12M&feature=youtu.be)

# **Testando o ipfs-mini com Node.JS**

Vamos testar esse pequeno módulo, que serve para consultar um nó IPFS, baseado no [browser-ipfs](https://github.com/pelle/browser-ipfs).

Usei esse repositório como referência para realizar esse tutorial: [ipfs-mini](https://github.com/SilentCicero/ipfs-mini)

Dúvidas sobre a utilizaçãod o fs: [fs](https://nodejs.org/api/fs.html)

## Instalação

Iniciar nossa instalação, primeiramente iniciando o **npm** no repositório de sua escolha.

    npm init
Agora vamos precisar baixar as dependências do nosso projeto, a primeira é o **ipfs-mini**.

    npm i ipfs-mini -save
Em seguida, vamos precisar do do fs para manipular arquivos, que vão ser servidos em nossa estrutura IPFS local.

    npm i fs -save
 O comando **-save**  é para as dependências ja serem adicionados em nosso repositório. 

## Criando arquivos

Agora vamos criar dois arquivos em nosso diretório, um arquivos javascript, e um arquivo txt.

![](img/diretorio-inicial.PNG)

No arquivo **txt.txt** escreva qualquer coisa, para que possamos identifica-lo.

![](img/conteudo-txt.png)

No arquivo **ipfs.js** usaremos o seguinte código.
Importaremos os módulos: ipfs-mini e fs

![](img/codigo-principal.png)

## Executando

Agora executar o arquivo ipfs.js, digite o seguinte comando no console.

    node ipfs
Vamos ter a seguinte saída se tudo tiver dado certo.

![](img/node-ipfs.png)

Veja que foi criado um hash
**Qmakaipv16UTwLVNhVprHWdELrFWshPmTRwLkXV1AvzNaW**

E também um link para acessar o arquivo dentro do IPFS.
[https://ipfs.infura.io/ipfs/Qmakaipv16UTwLVNhVprHWdELrFWshPmTRwLkXV1AvzNaW](https://ipfs.infura.io/ipfs/Qmakaipv16UTwLVNhVprHWdELrFWshPmTRwLkXV1AvzNaW)

Dê um ctrl+click em cima do link, ou copie e cole em seu navegador, e vamos conferir se o arquivo está disponível pelo ipfs.

![](img/resultado-navegador.png)
Aqui podemos ver que deu certo.

Agora vamos utilizar o método cat() do ipfs, que nos permite através do hash buscar o arquivo. Vamos acrescentar o seguinte código, passando a hash gerada.

![](img/cat.png)

Salve o arquivo, execute novamente.

    node ipfs

![](img/cat-resposta.png)

Veja que ele realizou a mesma função do navegador, e utilizou o gateway do ipfs-mini para localizar o arquivo, apresentá-lo no console.